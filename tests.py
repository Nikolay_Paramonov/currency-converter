import unittest
from currency_converter import CurrencyConverter


class TestCurrencyConverter(unittest.TestCase):

    def test_normal(self):
        converters = CurrencyConverter('http://data.fixer.io/api/latest?access_key=344b630b265ebb2ec745f92534d170a5')
        con = converters.convert('USD', 'RUB', 1)
        self.assertEqual(con, '1 USD = 80.28 RUB')
        con = converters.convert('USD', 'RUB', 0)
        self.assertEqual(con, '0 USD = 0.0 RUB')


if __name__ == '__main__':
    unittest.main()
