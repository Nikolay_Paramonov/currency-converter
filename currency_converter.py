"""Программа для конвертации валюты"""
import requests


class CurrencyConverter:
    """Пустой словарь для хранения коэффициентов конверсии"""
    rates = {}

    def __init__(self, url):
        """Извлечение только данных из данных JSON"""
        data = requests.get(url).json()
        self.rates = data["rates"]

    def convert(self, from_currency, to_currency, amount):
        """Метод для умножения суммы и показателя конверсии"""
        initial_amount = amount
        """Ограничение точности до 2 десятичных знаков"""
        amount = round(amount * self.rates[to_currency], 2)
        results = '{} {} = {} {}'.format(initial_amount, from_currency, amount, to_currency)
        return results


if __name__ == '__main__':
    url = str.__add__('http://data.fixer.io/api/latest?access_key=', '344b630b265ebb2ec745f92534d170a5')
    converter = CurrencyConverter(url)
    from_country = 'USD'
    to_country = 'RUB'
    while True:
        try:
            amount = int(input("Введите сумму: "))
            if int(amount) > 0:
                break
            else:
                print('Вы ввели ноль или отрицательное число. Попробуйте снова!')
        except:
            print('Не правильно введен формат данных. Попробуйте снова!')

    con = converter.convert(from_country, to_country, amount)
    print(con)
